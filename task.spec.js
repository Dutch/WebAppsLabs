/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

var processString = Task.processString;
var makeTaskFromObject = Task.fromObject;
var makeNewTask = Task.new;
var makeTaskFromString = Task.fromString;

// // ADD YOUR TESTS HERE
describe('Tests to see if the functions are there', function() {
   it('has a makeNewTask function', function() {
   	expect(makeNewTask).to.be.a('function');
   });
   it('has a makeTaskFromObject function', function() {
   	expect(makeTaskFromObject).to.be.a('function');
   });
   it('has a makeTaskFromString function', function() {
   	expect(makeTaskFromString).to.be.a('function');
   });
});

describe('Values for a new Task', function() {
	var test1 = Task.new();
	var test2 = Task.new();
	it('return an object', function() {
		expect(test1).to.be.a('object')
	});
	it('the first task has the id 1', function() {
   		expect(test1.id).equals(1);
    });
	it('the second task has the id 2', function() {
   		expect(test2.id).equals(2);
   	});
	it('the initial title is an empty string', function() {
   		expect(test1.title).equals('');
    });
    it('the initial completedTime is null', function() {
   		expect(test1.completedTime).equals(null);
    });
    var emptyarr = [];
    it('tags is initally an empty array', function() {
   		expect(test1.tags === emptyarr);
    });
});
describe('Tests for makeTaskFromObject', function() {
	var objTsk, obj;
	obj = {
		title: 'titleTest   ',
		tags: ["Rick", "Morty", "Birdperson"]
	}
	objTsk = Task.fromObject(obj);
	it('makeTaskFromObject Test: title', function() {
		expect(objTsk.title).to.equal('titleTest');
    });
    it('makeTaskFromObject Test: tags', function() {
		expect(objTsk.tags).to.equal(obj.tags);
    });
});
describe('Tests for makeTaskFromString', function() {
	var strTsk, str, list;
	list = ['hottopic', 'stuff'];
	str = " hi there! #hottopic #stuff "
	strTsk = Task.fromString(str);
	it('makeTaskFromString Test: title', function() {
		expect(strTsk.title).to.equal('hi there!');
    });
    it('makeTaskFromObject Test: tags', function() {
		expect(strTsk.tags.length).to.equal(list.length);
    });
});
describe('Tests for the proto methods', function() {
   var test1 = Task.new();
   var test2 = Task.new();
   it('Set title works with extra spaces', function() {
   		test2.prototype.setTitle.call(test2, '   spaces    ');
   		expect(test2.title).to.equal('spaces');
    });
   test1.prototype.setTitle.call(test1,'title');
   it('Set title works', function() {
   		expect(test1.title).equals('title');						//To test to see if setTitle works 	
    });
   it('is completed works', function() {
   		expect(test1.prototype.isCompleted.call()).equals(false);
    });
   var test2 = Task.new();
    it('toggle Completed works', function() {
   		test2.prototype.toggleCompleted.call(test2);			//this is changing completed time to the date
   		expect(test2.completedTime).to.not.equal(null);
    });
    it('toggle Completed works', function() {
   		test2.prototype.toggleCompleted.call(test2);			//this is going back to null the second time
   		expect(test2.completedTime).to.equal(null);
    });
    it('addTag adds a tag', function() {
    	test1.prototype.addTag.call(test1, 'shoes');
   		expect(test1.tags.length).equals(1);				//this checks that the length increases
    });
    it('addTag adds the right tag', function() {
    	test1.prototype.addTag.call(test1, 'shirt');
   		expect(test1.tags[test1.tags.length-1]).equals('shirt');				//this checks that the last tag was the one added
    });
    it('addTags Test: length', function() {						//tests add tags for length
    	var tagsTest, list;
    	tagsTest = Task.new();
    	list = ['cat', 'dog', 'iguana'];
    	tagsTest.prototype.addTags.call(tagsTest, list);
   		expect(tagsTest.tags.length).equals(3);
    });
    it('addTags Test: items', function() {						//tests add tags for items
    	var tagsTest, list;
    	tagsTest = Task.new();
    	list = ['cat', 'dog', 'iguana'];
    	tagsTest.prototype.addTags.call(tagsTest, list);
   		expect(tagsTest.tags).to.deep.equal(list);
    });	
    it('hasTag Test: true', function() {
   		expect(test1.prototype.hasTag.call(test1, 'shirt')).equals(true);				//test1 has the shirt that we just added
    });
    it('hasTag Test: false', function() {
   		expect(test1.prototype.hasTag.call(test1, 'sunglasses')).equals(false);				//test1 doesnt have sunglasses
    });
    it('removetag Test', function() {
    	test1.prototype.removeTag.call(test1, 'shirt');								//this gets rid of our shirt
   		expect(test1.prototype.hasTag.call(test1, 'shirt')).equals(false);
    });
    var removeTest, list;
    	removeTest = Task.new();
    	list = ['cat', 'dog', 'iguana'];
    	removeTest.prototype.addTags.call(removeTest, list)
    	removeTest.prototype.addTag.call(removeTest, 'kittens')
    it('removetags Test: element 1 of a list', function() {
    	removeTest.prototype.removeTags.call(test1, list);								//this gets rid of our shirt
   		expect(test1.prototype.hasTag.call(test1, 'cat')).equals(false);
    });
    it('removetags Test: element 2 of a list', function() {
    	removeTest.prototype.removeTags.call(test1, list);								//this gets rid of our shirt
   		expect(test1.prototype.hasTag.call(test1, 'dog')).equals(false);
    });
    it('removetags Test: element 3 of a list', function() {
    	removeTest.prototype.removeTags.call(test1, list);								//this gets rid of our shirt
   		expect(test1.prototype.hasTag.call(test1, 'iguana')).equals(false);
    });
    it('toggle tag Test: adding ', function() {
    	test1.prototype.toggleTag.call(test1, 'shirt');								//adds the tag back
   		expect(test1.prototype.hasTag.call(test1, 'shirt')).equals(true);
    });	
    it('toggle tag Test: removing ', function() {
    	test1.prototype.toggleTag.call(test1, 'shirt');								//removes the tag
   		expect(test1.prototype.hasTag.call(test1, 'shirt')).equals(false);
    });	
    var cloneTest;
    cloneTest = test1.prototype.clone.call(test1);
    it('Clone Test: title', function() {							//tests to see if it has the same title
   		expect(cloneTest.title).equals(test1.title);
    });	
    it('Clone Test: isCompleted', function() {						//tests to see if it has the same isCompleted
   		expect(cloneTest.isCompleted).equals(test1.isCompleted);
    });	
    it('Clone Test: tags', function() {	
    	var cloneTest2;
	    test1.prototype.addTag.call(test2, "lion");
	    test1.prototype.addTag.call(test2, "tiger");
	    test1.prototype.addTag.call(test2, "bear");
	    cloneTest2 = test2.prototype.clone.call(test2);
   		expect(cloneTest2.tags).equals(test2.tags);				//tests to see if it has the same tags
   		expect(cloneTest2.tags.length).equals(3);
    });	
    
});

