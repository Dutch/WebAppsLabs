/*
 * task.js
* made by: Dutch Wyatt
 *
 * Contains implementation for a "task" "class"
 */
var Task, proto, idcount;

idcount = 1;
// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */

function makeNewTask() {
   var newTask;

   newTask = Object.create({
      id: idcount,
      title: '',
      completedTime: null,
      tags: []
   });
   idcount += 1;
   newTask.prototype = proto;
   Object.defineProperty(Task, 'id', {
      configurable : false,
      enumerable : true,
      writable : false
   });
   return newTask;
}

function makeTaskFromObject(o) {
   var result;
   result = Task.new();
   result.prototype.setTitle.call(result, o.title);
   result.tags = o.tags;
   return result;
}

function makeTaskFromString(str) {
   var result;
   str = str.trim();
   obj = processString(str);
   return (result = Task.fromObject(obj));

}

/*
 *       Prototype / Instance methods
 */

proto = {
   //Add instance methods here
   setTitle: function setTitle(s) {
      s = s.trim();
      this.title = s;

      return this;
   },
   isCompleted: function() {
      if (this.completedTime == null) {

         return false;
      }

      return true;
   },
   toggleCompleted: function() {
      if (this.completedTime == null) {
         this.completedTime = new Date();

         return this;
      }
      else{
         this.completedTime = null;

         return this;
      }
   },
   hasTag: function(s) {
      if (this.tags.includes(s) === true) {

         return true;
      }

      return false;
   },
   addTag: function(s) {
      if (this.tags.includes(s) === false) {
         this.tags[this.tags.length] = s;
      }
   },
   addTags: function(arr) {
      var i;
      for(i = 0; i < arr.length; i += 1) {
         var string = arr[i];
         this.prototype.addTag.call(this, string);
      }
      return this;
   },
   removeTags: function(arr) {
      var i;
      for(i = 0; i < arr.length; i += 1) {
         var string = arr[i];
         this.prototype.removeTag.call(this, string);
      }
      return this;
   },
   removeTag: function(s) {
      if (this.tags.includes(s)) {
         delete this.tags[this.tags.indexOf(s)];
      }

      return this;
   },
   toggleTag: function(s) {
      if (this.tags.includes(s)) {
         this.prototype.removeTag.call(this, s);
      }
      else {
         this.prototype.addTag.call(this, s);
      }

      return this;
   },
   clone: function() {
      var result;
      result = Task.new();
      result.title = this.title;
      result.isCompleted = this.isCompleted;
      result.tags = this.tags;

      return result;
   }



};
// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
